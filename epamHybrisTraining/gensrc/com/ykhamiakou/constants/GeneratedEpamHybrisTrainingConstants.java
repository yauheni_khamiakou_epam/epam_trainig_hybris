/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 30.10.2018 20:04:48                         ---
 * ----------------------------------------------------------------
 */
package com.ykhamiakou.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedEpamHybrisTrainingConstants
{
	public static final String EXTENSIONNAME = "epamHybrisTraining";
	
	protected GeneratedEpamHybrisTrainingConstants()
	{
		// private constructor
	}
	
	
}
